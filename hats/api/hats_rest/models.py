from django.db import models


class LocationVO(models.Model):
    """
    The Location VO provides basic information about location in

    the wardrobe, just href and closet_name.
    """

    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Hat(models.Model):
    """
    The Hat model describes the style name, the color,
    includes a picture and the location in the wardrobe.
    """

    name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.name
