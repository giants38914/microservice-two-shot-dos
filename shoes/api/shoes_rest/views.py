from django.shortcuts import render
from .models import Shoe, BinVO
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
    ]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "name",
        "color",
        "manufacturer",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


# @require_http_methods(["GET", "POST"])
# def api_list_shoes(request):
#     """
#     Collection RESTful API handler for Location objects in
#     the wardrobe.

#     GET:
#     Returns a dictionary with a single key "locations" which
#     is a list of the closet name, section number, and shelf
#     number for the location, along with its href and id.

#     {
#         "locations": [
#             {
#                 "id": database id for the location,
#                 "closet_name": location's closet name,
#                 "section_number": the number of the wardrobe section,
#                 "shelf_number": the number of the shelf,
#                 "href": URL to the location,
#             },
#             ...
#         ]
#     }

#     POST:
#     Creates a location resource and returns its details.
#     {
#         "closet_name": location's closet name,
#         "section_number": the number of the wardrobe section,
#         "shelf_number": the number of the shelf,
#     }
#     """
#     if request.method == "GET":
#         shoes = Shoe.objects.all()
#         return JsonResponse(
#             {"shoes": shoes},
#             encoder=ShoeEncoder,
#         )
#     else:
#         content = json.loads(request.body)
#         shoes = Shoe.objects.create(**content)
#         return JsonResponse(
#             shoes,
#             encoder=ShoeEncoder,
#             safe=False,
#         )


@require_http_methods(["DELETE"])
def api_delete_shoes(request, id):
    """
    Single-object API for the Bin resource.

    DELETE:
    Removes the bin resource from the application
    """
    if request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
