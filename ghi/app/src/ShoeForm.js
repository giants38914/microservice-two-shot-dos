import React, { useEffect, useState } from 'react';

function ShoeForm(props) {
    const [bin, setBin] = useState("");
    const [name, setName] = useState("");
    const [color, setColor] = useState("");
    const [manufacturer, setManufacturer] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [bins, setBins] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.manufacturer = manufacturer;
        data.bin = bin;
        data.color = color;
        data.picture_url = pictureUrl;
        console.log(data);

        const ShoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
        };

        const response = await fetch(ShoeUrl, fetchConfig);
        if (response.ok) {
        const newShoe = await response.json();
        console.log(newShoe);

        setName('');
        setColor('');
        setManufacturer('');
        setBin('');
        setPictureUrl('');
        }
    };

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    };
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    };
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    };

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe listing</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange}placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={manufacturer} onChange={handleManufacturerChange}placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                    <div className="form-floating mb-3">
                    <input value={pictureUrl} onChange={handlePictureUrlChange} required type="url" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a bin</option>
                    {bins.map(bin => {
                    return (
                    <option key={bin.id} value={bin.href}>
                    {bin.closet_name}
                    </option>
                    );
                })}
                </select>
                </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>

    );
    }

export default ShoeForm;
