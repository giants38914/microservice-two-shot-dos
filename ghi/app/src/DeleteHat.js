import React, { useEffect, useState } from 'react';

function DeleteHat(props) {
  const [hats, setHats] = useState([]);
  const [selectedHat, setSelectedHat] = useState('');


  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!selectedHat) {
      return;
    }

    const HatUrl = `http://localhost:8090/api/hats/${selectedHat}`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch(HatUrl, fetchConfig);
    if (response.ok) {
      // Hat deleted successfully, update the state or perform any necessary actions
      setSelectedHat('');
    }
  };

  const handleDeleteChange = (event) => {
    const value = event.target.value;
    setSelectedHat(value);
  };



  return (
    <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Delete a hat</h1>
                <form onSubmit={handleSubmit} id="delete-hat">

                <div className="mb-3">
                    <select onChange={handleDeleteChange} value={selectedHat} required name="hat" id="hat" className="form-select">
                    <option value="">Choose a hat</option>
                {hats.map(hat => {
                return (
                  <option key={hat.id} value={hat.id}>
                  {hat.name}
                  </option>
                );
              })}
            </select>
            </div>
                <button className="btn btn-primary">Delete</button>
                </form>
            </div>
            </div>
        </div>
    </div>

  );
}

export default DeleteHat;
