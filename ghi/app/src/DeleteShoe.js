import React, { useEffect, useState } from 'react';

function DeleteShoe(props) {
    const [shoes, setShoes] = useState([]);
    const [selectedShoe, setSelectedShoe] = useState('');


    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        if (!selectedShoe) {
        return;
        }

        const ShoeUrl = `http://localhost:8080/api/shoes/${selectedShoe}`;
        const fetchConfig = {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json',
        }
        };

        const response = await fetch(ShoeUrl, fetchConfig);
        if (response.ok) {
        // Hat deleted successfully, update the state or perform any necessary actions
        setSelectedShoe('');
        }
    };

    const handleDeleteChange = (event) => {
        const value = event.target.value;
        setSelectedShoe(value);
    };



    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Delete a hat</h1>
                    <form onSubmit={handleSubmit} id="delete-hat">

                    <div className="mb-3">
                        <select onChange={handleDeleteChange} value={selectedShoe} required name="shoe" id="shoe" className="form-select">
                        <option value="">Choose a shoe</option>
                    {shoes.map(shoe => {
                    return (
                    <option key={shoe.id} value={shoe.id}>
                    {shoe.name}
                    </option>
                    );
                })}
                </select>
                </div>
                    <button className="btn btn-primary">Delete</button>
                    </form>
                </div>
                </div>
            </div>
        </div>

    );
    }

export default DeleteShoe;
