import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";

function HatDetail() {
  const { id } = useParams();
  const [hat, setHat] = useState(null);

  useEffect(() => {
    async function fetchHat() {
      const response = await fetch(`http://localhost:8090/api/hats/${id}`);

      if (response.ok) {
        const data = await response.json();
        setHat(data);
      }
    }

    fetchHat();
  }, [id]);

  if (!hat) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h2>{hat.name}</h2>
      <p>Color: {hat.color}</p>
      <p>Fabric: {hat.fabric}</p>
      <p>Location: {hat.location.closet_name}</p>
      <img
        src={hat.picture_url}
        alt={hat.name}
        style={{ width: "400px", border: "4px solid #00b601" }}
      />
      <p></p>
      <p>Image source: <a href={hat.picture_url}>{hat.picture_url}</a></p>
    </div>
  );
}

export default HatDetail;
