function MainPage() {
  return (

    <div className="px-4 py-5 my-5 text-center">
      <img
        src={'https://cdn.create.vista.com/api/media/medium/464632570/stock-photo-trendy-couple-stylish-outfit-posing?token=://cdn.create.vista.com/api/media/medium/464632570/https://cdn.create.vista.com/api/media/medium/464632570/stock-photo-trendy-couple-stylish-outfit-posing?token=-photo-trendy-couple-stylish-outfit-posing?token=://marketplace.canva.com/EAFMsIjOwsA/1/0/1600w/canva-brown-minimalist-fashion-store-special-offer-banner-mE9LVlNkf_A.jpg'}
        alt="Home page"
        style={{
          width: "80%",
          border: "4px solid #00b601",
          display: "block",
          margin: "0 auto",
          borderRadius: "50%"
        }}
      />
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
    </div>
  );
}

export default MainPage;
