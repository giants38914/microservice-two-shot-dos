import React, { useEffect, useState } from "react";

function Shoes(props) {

console.log("test")
    const [shoes, setShoes] = useState([]);

    async function fetchShoes (){
        const response = await fetch("http://localhost:8080/api/shoes/");

        if (response.ok) {
        const data = await response.json();
        console.log(data);
        setShoes(data.shoes);
        }
    };

    useEffect(() => {
        fetchShoes();
    }, []);

    return (
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Name</th>
            <th>Color</th>
            <th>Manufacturer</th>
            {/* <th>Picture</th> */}
            </tr>
        </thead>
        <tbody>
            {shoes.map(shoe => {
            return (
                <tr key={shoe.href}>
                <td>{ shoe.name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.manufacturer }</td>
                <td>
                    <img
                        src={shoe.picture_url}
                        alt={shoe.name}
                        style={{
                            width: "150px",
                            borderRadius: "100%"
                        }}
                    />
                    </td>
                </tr>
            );
            })}
        </tbody>
        </table>
    );
}

export default Shoes;
