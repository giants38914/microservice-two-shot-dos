import React, { useEffect, useState } from 'react';

function HatForm(props) {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState("");
  const [color, setColor] = useState("");
  const [fabric, setFabric] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [location, setLocation] = useState("");

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.fabric = fabric;
    data.location = location;
    data.color = color;
    data.picture_url = pictureUrl;
    console.log(data);

    const HatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch(HatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);

      setName('');
      setColor('');
      setFabric('');
      setLocation('');
      setPictureUrl('');
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  };
  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  };

  return (
    <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat listing</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={color} onChange={handleColorChange}placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={fabric} onChange={handleFabricChange}placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                <div className="form-floating mb-3">
                <input value={pictureUrl} onChange={handlePictureUrlChange} required type="url" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                {locations.map(location => {
                return (
                  <option key={location.id} value={location.href}>
                  {location.closet_name}
                  </option>
                );
              })}
            </select>
            </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    </div>

  );
}

export default HatForm;
