import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Hats from './Hats';
import Shoes from './Shoes';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';
import DeleteHat from './DeleteHat';
import DeleteShoe from './DeleteShoe';
import HatDetail from "./HatDetail";
import NotFoundPage from './NotFoundPage';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<Hats />} />
          <Route path="/hats/new" element={<HatForm />} />
          <Route path="/hats/:id" element={<HatDetail />} />
          <Route path="/hats/delete" element={<DeleteHat />} />
          <Route path="/shoes" element={<Shoes />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/shoes/delete" element={<DeleteShoe/>} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
