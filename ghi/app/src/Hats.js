import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Hats(props) {

console.log("test")
  const [hats, setHats] = useState([]);

  async function fetchHats (){
    const response = await fetch("http://localhost:8090/api/hats/");

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setHats(data.hats);
    }
  };

  useEffect(() => {
    fetchHats();
  }, []);

  return (
    <div className="container App">
        <img
        src={'https://marketplace.canva.com/EAFMsIjOwsA/1/0/1600w/canva-brown-minimalist-fashion-store-special-offer-banner-mE9LVlNkf_A.jpg'}
        alt="Woman with hat"
        style={{
          width: "80%",
          border: "4px solid #00b601",
          display: "block",
          margin: "0 auto"
        }}
      />

      <table className="table table-success table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Color</th>
          <th>Fabric</th>
          <th>Hat Image</th>
        </tr>
        </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.id}>
              <td>
                  <Link to={`/hats/${hat.id}`}>{hat.name}</Link>
                </td>
              <td>{hat.color}</td>
              <td>{hat.fabric}</td>
              <td>
                <Link to={`/hats/${hat.id}`}>
                  <img
                    src={hat.picture_url}
                    alt={hat.name}
                    style={{ width: "165px", border: "4px solid #00b601" }}
                  />
                </Link>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </div>
);
}

export default Hats;
